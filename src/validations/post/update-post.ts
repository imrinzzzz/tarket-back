import Joi from "joi";

type UpdatedPost = {
  postTitle?: string;
  postDescription?: string;
  discountPrice?: number;
  fullPrice?: number;
  postCategory?: number;
  locationId?: number;
}

export const updatePost = (postObj: UpdatedPost) => {
  const schema = Joi.object({
    postTitle: Joi.string(),
    postDescription: Joi.string(),
    discountPrice: Joi.number(),
    fullPrice: Joi.number(),
    postCategory: Joi.number(),
    locationId: Joi.number(),
  });

  const { error } = schema.validate(postObj);

  if (error) return error.details;
  return true;
};
