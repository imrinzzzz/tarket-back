import { createNewPost } from "./create-post";
import { updatePost } from "./update-post";

export default { createNewPost, updatePost };
