import Joi from "joi";

export const createNewPost = (postObj: IPost) => {
  const schema = Joi.object({
    postTitle: Joi.string().required(),
    postDescription: Joi.string().required(),
    discountPrice: Joi.number(),
    fullPrice: Joi.number().required(),
    postCategory: Joi.number().required(),
    postDatetime: Joi.date().required(),
    googleId: Joi.string().required(),
    locationId: Joi.number().required(),
    postPicture: Joi.array(),
  });

  const { error } = schema.validate(postObj);

  if (error) return error.details;
  return true;
};
