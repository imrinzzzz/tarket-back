import Joi from "joi";

export const facebookLogin = (userObj: any) => {
  const schema = Joi.object({
    phoneNumber: Joi.string().min(10).max(10).required(),
    verificationStatus: Joi.string().required(),
    bankAccNo: Joi.string().required(),
    bankAccName: Joi.string().required(),
    bankAccCompany: Joi.string().required(),
  });

  const { error } = schema.validate(userObj);

  if (error) return error.details;
  return true;
};
