import Joi from "joi";

export const createRefund = (refundObj: IRefund) => {
  const schema = Joi.object({
    userId: Joi.string().required(),
    message: Joi.string().required(),
    orderId: Joi.number().integer().required(),
  });

  const { error } = schema.validate(refundObj);

  if (error) return error.details;
  return true;
};
