import Joi from "joi";

export const createOrder = (orderObj: IOrder) => {
  const schema = Joi.object({
    orderDatetime: Joi.date().required(),
    orderStatus: Joi.string().valid(
      "created",
      "paid",
      "delivered",
      "received",
      "failed",
      "refunded"
    ),
    googleId: Joi.string().required(),
    postId: Joi.number().required(),
    addressName: Joi.string().required(),
    deliveryName: Joi.string(),
    totalPrice: Joi.number().required(),
    deliveryFee: Joi.number(),
    discountPrice: Joi.number(),
  });

  const { error } = schema.validate(orderObj);

  if (error) return error.details;
  return true;
};
