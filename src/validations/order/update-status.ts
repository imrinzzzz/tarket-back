import Joi from "joi";

export const updateStatus = (orderStatus: orderstatus) => {
  const schema = Joi.object({
    orderStatus: Joi.string()
      .valid("created", "paid", "delivered", "received", "failed", "refunded")
      .required(),
  });

  const { error } = schema.validate({ orderStatus });

  if (error) return error.details;
  return true;
};
