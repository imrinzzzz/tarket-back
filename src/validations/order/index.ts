import { updateStatus } from "./update-status";
import { createOrder } from "./create-order";

export default { createOrder, updateStatus };
