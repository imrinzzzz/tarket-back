import Joi from "joi";

export const chargeToken = (paymentObj: IPayment) => {
  const schema = Joi.object({
    token: Joi.string().required(),
    description: Joi.string(),
    amount: Joi.number().required().min(1).max(150000).precision(2),
  });

  const { error } = schema.validate(paymentObj);

  if (error) return error.details;
  return true;
};
