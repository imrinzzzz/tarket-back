import Joi from "joi";

export const createNewUsers = (userObj: IUser) => {
  const schema = Joi.object({
    phoneNumber: Joi.string()
      .min(10)
      .max(10)
      .pattern(/^[0-9]+$/),
    username: Joi.string().min(2).max(50).required(),
    verificationStatus: Joi.string().required(),
    firstName: Joi.string().pattern(/^\w+$/).required(),
    lastName: Joi.string().pattern(/^\w+$/).required(),
    bankAccNo: Joi.string(),
    bankAccName: Joi.string(),
    bankAccCompany: Joi.string(),
    address: Joi.array(),
  });

  const { error } = schema.validate(userObj);

  if (error) return error.details;
  return true;
};
