import { createNewUsers } from "./create-new-users";
import { checkEmail } from "./check-email";

export default { createNewUsers, checkEmail };
