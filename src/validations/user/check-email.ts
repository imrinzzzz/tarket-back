import Joi from "joi";

export const checkEmail = (email: string) => {
  const schema = Joi.object({
    email: Joi.string().email({
      minDomainSegments: 2,
      tlds: { allow: ["com", "net", "co", "th"] },
    }),
  });

  const { error } = schema.validate({ email });

  if (error) return error.details;
  return true;
};
