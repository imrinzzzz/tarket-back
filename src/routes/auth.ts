import express from "express";
import AuthController from "../controller/auth";

const router = express.Router();

// Facebook login
router.post("/facebook", AuthController.facebookLogin);

export default router;
