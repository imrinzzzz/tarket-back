import express from 'express';
import multer from 'multer'
import UserController from '../controller/user';
import checkIfAuthenticated from '../middleware/is-authenticated'

const router = express.Router();
const upload = multer()

// check if user exists in postgresql
// if not create new user with provided email
router.post('/logIn-singUp', checkIfAuthenticated, upload.none(), UserController.checkCreateUser);

// update user
router.post('/update', checkIfAuthenticated, upload.none(), UserController.updateUsers);

// check if user exists
router.get('/isExist', UserController.checkIfExisted);

// upload profile pictureÍ
router.post('/upload-pfp', checkIfAuthenticated, upload.single('file'), UserController.addProfilePicture)

// get user detail
router.get('/', checkIfAuthenticated, UserController.getUserDetail)

// get user's address
router.get('/address', checkIfAuthenticated, UserController.getUserAddress)

export default router;
