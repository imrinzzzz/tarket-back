import express from "express";
import checkIfAuthenticated from "../middleware/is-authenticated";
import RefundController from "../controller/refund";

const router = express.Router();

// create refund
router.post("/create", checkIfAuthenticated, RefundController.createRefund);

// get all refunds
router.get("/", RefundController.getRefunds)

export default router;
