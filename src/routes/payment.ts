import express from "express";
import checkIfAuthenticated from "../middleware/is-authenticated";
import PaymentController from "../controller/payment";

const router = express.Router();

// chrage card with given token
router.post("/checkout/:orderId", checkIfAuthenticated, PaymentController.chargeToken);

export default router;
