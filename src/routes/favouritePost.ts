import express from "express";
import checkIfAuthenticated from "../middleware/is-authenticated";
import FavPostController from "../controller/favouritePost";

const router = express.Router();

router.post(
  "/add-remove",
  checkIfAuthenticated,
  FavPostController.favouritePost
);

router.get("/", checkIfAuthenticated, FavPostController.favouriteFromUid);

router.get(
  "/post",
  checkIfAuthenticated,
  FavPostController.favouritePostsFromUid
);

export default router;
