import express from "express";
import Postcontroller from "../controller/post";
import multer from "multer";
import checkIfAuthenticated from "../middleware/is-authenticated";

const router = express.Router();
const upload = multer();

// get all posts
router.get("/", Postcontroller.getPosts);

// get post from id
router.get("/id/:id", checkIfAuthenticated, Postcontroller.postFromId);

// get post from id, NO AUTHEN
router.get("/no-authen", Postcontroller.postFromIdNoAuthen);

// get post from category
router.get(
  "/category/:category",
  checkIfAuthenticated,
  Postcontroller.postFromCate
);

// get post from location
router.get(
  "/location/:locId",
  checkIfAuthenticated,
  Postcontroller.postFromLoc
);

// get post from user id
router.get("/uid", checkIfAuthenticated, Postcontroller.postFromUid);

// create post
router.post(
  "/create",
  checkIfAuthenticated,
  upload.array("image"),
  Postcontroller.createPost
);

// update post
router.post("/update", checkIfAuthenticated, Postcontroller.updatePost);

// find post from search query
router.get("/search", checkIfAuthenticated, Postcontroller.searchPosts);

export default router;
