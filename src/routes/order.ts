import express from "express";
import checkIfAuthenticated from "../middleware/is-authenticated";
import OrderController from "../controller/order";

const router = express.Router();

// create order
router.post("/create", checkIfAuthenticated, OrderController.createOrder);

// get order from id
router.get("/find", checkIfAuthenticated, OrderController.orderFromId);

// get order from user id
router.get("/find", checkIfAuthenticated, OrderController.orderFromUid);

// update order status
router.post("/update", checkIfAuthenticated, OrderController.updatedOrder);

// add tracking no.
router.post("/add-trackno", checkIfAuthenticated, OrderController.addTrackingNo);

export default router;
