import express from "express";
import { getLoc } from "../controller/location";

const router = express.Router();

router.get("/", getLoc);

export default router;
