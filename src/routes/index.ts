import express from "express";

import UserRouter from "./user";
import AuthRouter from "./auth";
import PostRouter from "./post";
import LocationRouter from "./location";
import CateRouter from "./category";
import OrderRouter from "./order";
import RefundRouter from "./refund";
import PaymentRouter from "./payment";
import FavouriteRouter from "./favouritePost";

const router = express.Router();

router.use("/user", UserRouter);
router.use("/auth", AuthRouter);
router.use("/post", PostRouter);
router.use("/location", LocationRouter);
router.use("/categories", CateRouter);
router.use("/order", OrderRouter);
router.use("/refund", RefundRouter);
router.use("/payment", PaymentRouter);
router.use("/favourite", FavouriteRouter);

export const Router = router;
