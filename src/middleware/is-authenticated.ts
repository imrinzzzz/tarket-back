import admin from "./firebase-service";

const getAuthToken = (req: any, res: any, next: any) => {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "Bearer"
  ) {
    res.locals.authToken = req.headers.authorization.split(" ")[1];
    res.locals.email = req.body.email;
  } else {
    res.locals.authToken = null;
    res.locals.email = null;
  }
  return next();
};

const checkIfAuthenticated = (req: any, res: any, next: any) => {
  getAuthToken(req, res, async () => {
    try {
      const { authToken } = res.locals;
      const userInfo = await admin.auth().verifyIdToken(authToken);

      res.locals.currentUser = userInfo.uid;
      return next();
    } catch (e) {
      console.log(e);
      return res.status(401).json({
        message: "You are not authorized to make this request.",
        error: e,
      });
    }
  });
};

export default checkIfAuthenticated;
