require("dotenv").config();

export const config = {
  env: process.env.NODE_ENV || "development",
  server: {
    port: process.env.PORT || 8080,
  },
  google: {
    projectId: process.env.PROJ_ID!,
    bucket: process.env.STORE_BUCKET!,
    bucketFolder: process.env.BUCKET_FOLDER!,
    apiKey: process.env.API_KEY!,
    appId: process.env.APP_ID!,
    authDomain: process.env.AUTH_DOMAIN!,
    databaseURL: `https://${process.env.PROJ_ID!}.firebaseio.com`,
  },
};
