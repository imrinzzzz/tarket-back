import admin from "firebase-admin";

require("dotenv").config();

admin.initializeApp({
  credential: admin.credential.cert(
    process.env.GOOGLE_APPLICATION_CREDENTIALS!
  ),
  databaseURL: `https://${process.env.PROJ_ID}.firebaseio.com`,
});

export default admin;
