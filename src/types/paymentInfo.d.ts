type IPaymentInfo = {
  paymentInfoName: string;
  paymentType: string;
  paymentNo: string;
  paymentName: string;
}
