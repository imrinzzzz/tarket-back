type IPayment = {
  token: any;
  amount: number;
  description: string;
}
