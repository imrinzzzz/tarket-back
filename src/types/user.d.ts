declare enum verificationstatus {
  ready='ready',
  done='done',
}

type IUser = {
  phoneNumber: string;
  username: string;
  verificationStatus: verificationstatus;
  profilePicture?: Buffer;
  bankAccNo?: string;
  bankAccName?: string;
  bankAccCompany?: string;
  address?: object;
  firstName: string;
  lastName: string;
};
