declare enum orderstatus {
  created='created',
  paid='paid',
  delivered='delivered',
  received='received',
  failed='failed',
  refunded='refunded',
}

type IOrder = {
  orderId: number;
  orderDatetime: Date;
  orderStatus?: string;
  googleId: string;
  postId: number;
  addressName: string;
  deliveryName?: string;
  totalPrice: number;
  deliveryFee?: number;
  discountPrice?: number;
}
