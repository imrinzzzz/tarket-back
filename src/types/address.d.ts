type IAddress = {
  addressName: string;
  houseNumber: string;
  street: string;
  province: string;
  district: string;
  subDistrict: string;
  zip: string;
}
