type IPost = {
  postTitle: string;
  postDescription: string;
  discountPrice?: number;
  fullPrice: number;
  postCategory: number;
  postDatetime: Date;
  googleId: string;
  locationId: number;
  postPicture?: string[];
}
