import { addTrackingNo } from './add-tracking';
import { updatedOrder } from "./update-status";
import { createOrder } from "./create-order";
import { orderFromId } from "./order-from-id";
import { orderFromUid } from "./order-from-userId";

export default { createOrder, orderFromId, orderFromUid, updatedOrder, addTrackingNo };
