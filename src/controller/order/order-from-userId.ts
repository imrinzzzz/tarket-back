import { prisma } from "../../middleware/prisma";

type IReq = {
  query: {
    uid: string;
    orderStatus?: orderstatus;
  };
};

export const orderFromUid = async (req: IReq, res: any, next: any) => {
  try {
    const { order } = prisma;
    const { uid, orderStatus } = req.query;

    if (!uid) return next();
    var theOrder;

    if (orderStatus) {
      theOrder = await order.findMany({
        where: {
          googleId: uid,
          orderStatus: orderStatus,
          NOT: {
            orderStatus: "received",
          },
        },
        orderBy: {
          orderDatetime: "desc",
        },
        select: {
          orderId: true,
          Post: {
            select: {
              postPicture: true,
            },
          },
        },
      });
    } else {
      theOrder = await order.findMany({
        where: {
          googleId: uid,
          NOT: {
            orderStatus: "received",
          },
        },
        select: {
          orderId: true,
          Post: {
            select: {
              postPicture: true,
            },
          },
        },
      });
    }

    if (theOrder) {
      return res.status(200).json(
        theOrder.map((order) => {
          return {
            orderId: order.orderId,
            postPicture: order.Post?.postPicture,
          };
        })
      );
    } else {
      return res.status(404).json({ message: "No order was found" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
