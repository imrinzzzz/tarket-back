import { prisma } from "./../../middleware/prisma";

type IReq = {
  body: {
    trackingNo: string;
    deliveryName: string;
  };
  query: {
    postId: number;
  };
};

export const addTrackingNo = async (req: IReq, res: any, _next: any) => {
  try {
    const { post } = prisma;
    const { trackingNo, deliveryName } = req.body;
    const { postId } = req.query;

    const updatedOrder = await post.update({
      where: {
        postId: +postId,
      },
      data: {
        Order: {
          updateMany: {
            where: {
              orderStatus: "paid",
            },
            data: {
              trackingNo: trackingNo,
              deliveryName: deliveryName,
              orderStatus: "delivered",
            },
          },
        },
      },
      include: {
        Order: true,
      },
    });
    if (updatedOrder) {
      return res.status(200).json({
        message: "Successfully added tracking number and delivery name! ",
        order: updatedOrder,
      });
    } else {
      return res.status(400).json({
        message: "Failed to add tracking number... xox",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
