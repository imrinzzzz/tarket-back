import { prisma } from "../../middleware/prisma";
import OrderValidator from "../../validations/order";
import { deleteImageFromGCS } from "./delete-image";

type IReq = {
  query: {
    orderId: number;
  };
  body: {
    orderStatus: orderstatus;
  };
};

export const updatedOrder = async (req: IReq, res: any, _next: any) => {
  try {
    const { order, post } = prisma;
    const { orderId } = req.query;
    const { orderStatus } = req.body;

    const validated = OrderValidator.updateStatus(orderStatus);

    if (typeof validated === "object")
      return res.status(422).json({
        message: "You may input wrong order status",
        error: validated,
      });

    const theOrder = await order.update({
      where: {
        orderId: +orderId,
      },
      data: {
        orderStatus: orderStatus,
      },
    });

    if (theOrder) {
      if (orderStatus === "received") {
        if (theOrder.postId) {
          const pictureToBeDeleted = await post.findUnique({
            where: {
              postId: theOrder.postId,
            },
            select: {
              postPicture: true,
            },
          });
          if (pictureToBeDeleted) {
            pictureToBeDeleted.postPicture.map((picture) => {
              deleteImageFromGCS(picture).catch(console.error);
            });
          }
          const deletPost = await post.delete({
            where: {
              postId: theOrder.postId,
            },
          });
          if (deletPost) {
            return res.status(200).json({
              message: `Successfully changed order status to ${theOrder.orderStatus} and successfully delete postId: ${theOrder.postId}`,
            });
          }
        } else {
          return res.status(200).json({
            message: `Successfully changed order status to ${theOrder.orderStatus}, but no post existed`,
          });
        }
      } else if (orderStatus === "paid") {
        if (theOrder.postId) {
          const updatedPost = await post.update({
            where: {
              postId: theOrder.postId,
            },
            data: {
              isPaid: true,
            },
          });
          if (updatedPost) {
            return res.status(200).json({
              message: `Successfully changed order status to ${theOrder.orderStatus} and updated post status (isPaid) to ${updatedPost.isPaid}`,
            });
          }
        }
      }

      return res.status(200).json({
        message: `Successfully changed order status to ${theOrder.orderStatus}`,
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
