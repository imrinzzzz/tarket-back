import { prisma } from "../../middleware/prisma";

type IReq = {
  query: {
    id: string;
  };
};

export const orderFromId = async (req: IReq, res: any, next: any) => {
  try {
    const { order } = prisma;
    const { id } = req.query;

    if (!id) return next();

    const theOrder = await order.findUnique({
      where: {
        orderId: parseInt(id),
      },
    });

    if (theOrder) {
      return res.status(200).json(theOrder);
    } else {
      return res.status(404).json({ message: "No order was found" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
