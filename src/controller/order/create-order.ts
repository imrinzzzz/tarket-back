import { prisma } from "../../middleware/prisma";
import OrderValidator from "../../validations/order";

type IReq = {
  body: IOrder;
};

export const createOrder = async (req: IReq, res: any, _next: any) => {
  try {
    const { order, user, post } = prisma;
    const { body } = req;

    // validate post
    const validated = OrderValidator.createOrder(body);
    if (typeof validated === "object")
      return res.status(422).json({
        message: "You may input wrong information",
        error: validated,
      });

    const userExisted = user.findFirst({
      where: {
        googleId: body.googleId,
      },
    });

    const postExisted = post.findFirst({
      where: {
        postId: body.postId,
      },
    });

    if (!(userExisted && postExisted))
      return res.status(404).json({
        message:
          "Post, User, or Address information does not exist or is incorrect",
      });

    const newOrder = await order.create({
      data: {
        ...body,
        orderDatetime: new Date(body.orderDatetime),
        orderStatus: "created",
      },
    });

    return res.status(201).json({
      message: "Created order successfully!",
      order: newOrder,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
