import { config } from "../../middleware/firebase-storage";
import { Storage } from "@google-cloud/storage";

export const deleteImageFromGCS = async (linkToPicture: string) => {
  const storage = new Storage({
    projectId: config.google.projectId,
    keyFilename: process.env.GOOGLE_APPLICATION_CREDENTIALS,
  });
  const bucket = storage.bucket(config.google.bucket);
  const regex = /(stage|prod)\/[\w\d-]+.\w+/;
  let fileName = linkToPicture.match(regex) || "";
  let file = bucket.file(fileName[0]);
  await file.delete()
};
