import { prisma } from "../middleware/prisma";

export const getCategory = async (_req: any, res: any, _next: any) => {
  try {
    const { postCategory } = prisma;
    const allCategories = await postCategory.findMany();
    if (allCategories) {
      return res.status(200).json(allCategories);
    } else {
      return res.status(404).json({ message: "No category was found" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem! ", error });
  }
};
