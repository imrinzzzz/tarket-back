import axios from "axios";
import { v4 as uuid } from "uuid";
import {prisma} from "../../middleware/prisma";
import AuthValidator from "../../validations/auth";

type IReq = {
  body: any;
  query: {
    accessToken: string;
  };
}

export const facebookLogin = async (req: IReq, res: any, _next: any) => {
  try {
    const { user } = prisma;
    const { body, query } = req;
    let existedUser;

    // validate user
    const validated = AuthValidator.facebookLogin(body);
    if (typeof validated === "object")
      return res.status(422).json({
        message: "You may input wrong information",
        error: validated,
      });

    // get facebook user login by user's access_token
    const fbUser = await axios.get(
      `${process.env.FACEBOOK_API_URL}/me?access_token=${query.accessToken}&fields=name,email,picture`
    );

    // check if user already login by fb
    if (fbUser.data.email) {
      existedUser = await user.findFirst({
        where: { email: fbUser.data.email },
      });
    } else {
      existedUser = await user.findFirst({
        where: { facebookId: fbUser.data.id },
      });
    }

    if (!existedUser) {
      // create user
      const data = {
        userId: uuid(),
        email: fbUser.data.email,
        username: fbUser.data.name,
        profilePicture: fbUser.data.picture.data.url,
        facebookId: fbUser.data.id,
        ...body,
      };

      existedUser = await user.create({ data });
    } else {
      // update existing user to add facebook id
      await user.update({
        where: { userId: existedUser.userId },
        data: { facebookId: fbUser.data.id },
      });
    }

    return res
      .status(200)
      .json({ message: "Facebook login successful", user: existedUser });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has problem!", error });
  }
};
