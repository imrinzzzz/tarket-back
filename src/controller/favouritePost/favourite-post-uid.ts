import { prisma } from "./../../middleware/prisma";

type IReq = {
  query: {
    uid: string;
  };
};

export const favouritePostsFromUid = async (
  req: IReq,
  res: any,
  _next: any
) => {
  try {
    const { favouritePost } = prisma;
    const { uid } = req.query;

    const posts = await favouritePost.findMany({
      where: {
        googleId: uid,
        Post: {
          isPaid: false,
        },
      },
      orderBy: {
        Post: {
          postDatetime: "desc",
        },
      },
      select: {
        Post: {
          include: {
            User: {
              select: {
                verificationStatus: true,
              },
            },
            Order: {
              select: {
                orderId: true,
              },
            },
          },
        },
      },
    });

    if (posts) {
      return res.status(200).json(
        posts.map((post) => {
          let { User, Order, ...postInfo } = post.Post!;
          return {
            ...postInfo,
            verificationstatus: User?.verificationStatus,
            orderId: Order.map(({ orderId }) => orderId),
            isFav: true,
          };
        })
      );
    } else {
      return res.status(404).json({
        message: "No favourite list of this user found!",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
