import { addRemoveFavourite } from "./add-remove-fav-subfunc";
import { prisma } from "../../middleware/prisma";

type IReq = {
  body: {
    postId: number;
  };
};

export const favouritePost = async (req: IReq, res: any, _next: any) => {
  try {
    const { favouritePost, post, user } = prisma;
    const { postId } = req.body;
    const { currentUser } = res.locals;
    const postExist = await post.findUnique({
      where: {
        postId: +postId,
      },
    });
    const userExist = await user.findUnique({
      where: {
        googleId: currentUser,
      },
    });
    if (postExist && userExist) {
      const isFav = await favouritePost.findUnique({
        where: {
          FavouritePost_googleId_postId_key: {
            postId: +postId,
            googleId: currentUser,
          },
        },
      });
      const action: string = isFav ? "remove" : "add";
      const favThePost = await addRemoveFavourite(postId, currentUser, action);
      if (favThePost) {
        return res.status(200).json({
          message: `Successfully ${action}ed favourite!`,
          favouritePost: favThePost,
        });
      }
    } else {
      return res.status(404).json({
        message: "Post or User not found..",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
