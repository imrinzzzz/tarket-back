import { favouritePostsFromUid } from "./favourite-post-uid";
import { favouritePost } from "./add-remove-favourite";
import { favouriteFromUid } from "./favourite-from-uid";

export default { favouritePost, favouriteFromUid, favouritePostsFromUid };
