import { prisma } from "./../../middleware/prisma";

export const addRemoveFavourite = (
  postId: number,
  googleId: string,
  action: string
) => {
  const { favouritePost } = prisma;
  const actions = {
    add: async (postId: number, googleId: string) => {
      return await favouritePost.create({
        data: {
          postId: postId,
          googleId: googleId,
        },
      });
    },
    remove: async (postId: number, googleId: string) => {
      return await favouritePost.delete({
        where: {
          FavouritePost_googleId_postId_key: {
            postId: postId,
            googleId: googleId,
          },
        },
      });
    },
  } as any;
  return (
    actions[action]?.(postId, googleId) ??
    "Can only add or remove favourite post.. "
  );
};
