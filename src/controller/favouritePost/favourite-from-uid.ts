import { prisma } from "./../../middleware/prisma";

type IReq = {
  query: {
    uid: string;
  };
};

export const favouriteFromUid = async (req: IReq, res: any, _next: any) => {
  try {
    const { favouritePost } = prisma;
    const { uid } = req.query;

    const returnList = await favouritePost.findMany({
      where: {
        googleId: uid,
      },
    });

    if (returnList) {
      return res.status(200).json(returnList);
    } else {
      return res.status(404).json({
        message: "No favourite list of this user found!",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
