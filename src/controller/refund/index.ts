import { createRefund } from "./create-refund";
import { getRefunds } from "./get-all-refunds";

export default { createRefund, getRefunds };
