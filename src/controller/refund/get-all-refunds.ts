import { prisma } from "../../middleware/prisma";

export const getRefunds = async (_req: any, res: any, _next: any) => {
  try {
    const { refund } = prisma;
    const refunds = await refund.findMany();
    const toBangkokTime = (val: any) => {
      val.datetime.setHours(val.datetime.getHours() + 7);
      return val;
    };
    console.log(refunds);
    if (refunds.length > 0 && refunds) {
      return res.status(200).json({
        refund: refunds.map(toBangkokTime),
      });
    } else {
      return res.status(404).json({ message: "no refund was found.." });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
