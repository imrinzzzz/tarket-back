import { prisma } from "../../middleware/prisma";
import RefundValidator from "../../validations/refund";

type IReq = {
  body: IRefund;
};

export const createRefund = async (req: IReq, res: any, _next: any) => {
  try {
    const { body } = req;
    const { refund, order } = prisma;

    const validated = RefundValidator.createRefund(body);
    if (typeof validated === "object")
      return res.status(422).json({
        message: "You may input wrong information",
        error: validated,
      });

    const newRefund = await refund.create({
      data: {
        ...body,
      },
    });

    if (newRefund) {
      const setOrderStatus = await order.update({
        where: {
          orderId: +body.orderId,
        },
        data: {
          orderStatus: "refunded",
        },
      });
      if (setOrderStatus) {
        return res.status(200).json({
          message: `successfully created refund and changed order status to ${setOrderStatus.orderStatus}`,
          refund: newRefund,
        });
      } else {
        return res.status(200).json({
          message: `successfully created refund and but failed to change order status..`,
          refund: newRefund,
        });
      }
    } else {
      return res.status(400).json({
        message: "failed to create refund..",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
