import PaymentValidator from "../../validations/payment";
import { prisma } from "../../middleware/prisma";

require("dotenv").config();

type IReq = {
  body: IPayment;
  params: {
    orderId: string | number;
  };
};

export const chargeToken = async (req: IReq, res: any, _next: any) => {
  try {
    const validated = PaymentValidator.chargeToken(req.body);

    if (typeof validated === "object")
      return res.status(422).json({
        message: "You may input wrong information",
        error: validated,
      });

    const { description, amount, token } = req.body;
    const { orderId } = req.params;
    const { order, post } = prisma;

    const theOrder = await order.findUnique({
      where: {
        orderId: +orderId,
      },
    });

    if (!theOrder)
      return res.status(404).json({
        message: "order not found!",
      });

    var omise = require("omise")({
      secretKey: process.env.OMISE_PRI,
      omiseVersion: "2015-09-10",
    });

    const charge = await omise.charges.create({
      description: description,
      amount: amount,
      currency: "thb",
      capture: true,
      card: token,
      metadata: {
        orderId,
      },
    });

    if (charge.authorized) {
      try {
        let orderCharge = null;
        if (charge.id) {
          orderCharge = await omise.charges.retrieve(charge.id);
        }
        if (orderCharge) {
          const updatedOrder = await order.update({
            where: {
              orderId: +orderId,
            },
            data: {
              orderStatus: "paid",
            },
          });
          const updatedPost = await post.update({
            where: {
              postId: updatedOrder.postId!,
            },
            data: {
              isPaid: true,
            },
          });
          if (updatedOrder && updatedPost) {
            return res.status(200).json({
              message: "order received successfully!",
              order: updatedOrder,
              postStatus: `isPaid = ${updatedPost.isPaid}`,
            });
          }
        }
      } catch (error) {
        return res.status(400).json({
          message: "an error has occurred during order received process..",
        });
      }
    } else if (charge.authorize_uri !== null) {
      res.redirect(charge.authorize_uri);
    } else {
      return res.status(400).json({
        message: "an error has occurred during charging process..",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
