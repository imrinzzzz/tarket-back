import { prisma } from "../../middleware/prisma";

export const postFromIdNoAuthen = async (req: any, res: any, _next: any) => {
  try {
    const { post } = prisma;

    const thePost = await post.findUnique({
      where: {
        postId: parseInt(req.query.id),
      },
      select: {
        postTitle: true,
        postDescription: true,
        discountPrice: true,
        fullPrice: true,
        postPicture: true,
      },
    });
    if (thePost) {
      return res.status(200).json(thePost);
    } else {
      return res.status(404).json({ message: "No post was found" });
    }
    // res.json(allUsers);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem! ", error });
  }
};
