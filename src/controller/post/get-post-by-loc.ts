import { prisma } from "../../middleware/prisma";

export const postFromLoc = async (req: any, res: any, _next: any) => {
  try {
    const { post, favouritePost } = prisma;
    const { locId } = req.params;
    const { currentUser } = res.locals;

    const posts = await post.findMany({
      where: {
        locationId: parseInt(locId),
        isPaid: false,
      },
      orderBy: {
        postDatetime: "desc",
      },
      include: {
        User: {
          select: {
            verificationStatus: true,
          },
        },
        Order: {
          select: {
            orderId: true,
          },
        },
      },
    });
    const favPost = await favouritePost.findMany({
      where: {
        googleId: currentUser,
      },
      select: {
        postId: true,
      },
    });
    const favPostArr = favPost.map(({ postId }) => postId);
    if (posts) {
      return res.status(200).json(
        posts.map((post) => {
          let { User, Order, ...newPost } = post;
          return {
            ...newPost,
            verificationStatus: post.User?.verificationStatus,
            orderId: post.Order.map(({ orderId }) => orderId),
            isFav: favPostArr.includes(post.postId),
          };
        })
      );
    } else {
      return res.status(404).json({ message: "No post was found" });
    }
  } catch (error) {
    return res.status(404).json({ message: "No post was found" });
  }
};
