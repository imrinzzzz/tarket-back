import { Storage } from "@google-cloud/storage";
import { createPostWithPrisma } from "./create-post-subfnc";
import mime from "mime-types";
import { v4 as uuid } from "uuid";
import { config } from "../../middleware/firebase-storage";
import { deleteImageFromGCS } from "../order/delete-image";

type IReq = {
  body: IPost;
  files: any;
};

require("dotenv").config();

export const createPost = async (req: IReq, res: any, _next: any) => {
  try {
    const { body, files } = req;
    var promises: Array<any> = new Array();

    if (files) {
      // upload to firebase storage
      const storage = new Storage({
        projectId: config.google.projectId,
        keyFilename: process.env.GOOGLE_APPLICATION_CREDENTIALS,
      });
      const bucket = storage.bucket(config.google.bucket);
      const BUCKET_FOLDER = config.google.bucketFolder;

      const getPublicUrl = (filename: string) => {
        return `https://storage.googleapis.com/${bucket}/${filename}`;
      };

      files.forEach((image: any, index: any) => {
        const type = mime.extension(files[index].originalname) || "jpeg";
        const gcsname = `${BUCKET_FOLDER}/${uuid()}.${type}`;
        const file = bucket.file(gcsname);

        const promise = new Promise((resolve: any, reject: any) => {
          const stream = file.createWriteStream({
            resumable: true,
            contentType: type,
            predefinedAcl: "publicRead",
          });

          stream.on("finish", async () => {
            try {
              image.cloudStorageObject = gcsname;
              await file.makePublic();
              image.cloudStoragePublicUrl = getPublicUrl(gcsname);
              resolve({ success: true, image: image.cloudStoragePublicUrl });
            } catch (err) {
              image.cloudStoragePublicUrl = err;
              reject({ success: false, error: err });
            }
          });

          stream.on("error", (err) => {
            image.cloudStoragePublicUrl = err;
            reject({ success: false, error: err });
          });

          stream.end(image.buffer);
        }).catch((err) => {
          return err;
        });

        promises.push(promise);
      });

      Promise.all(promises)
        .then((resolvedPromises) => {
          // check if there's error
          const checkIfErr = resolvedPromises.some((item) => {
            return !item.success;
          });
          if (checkIfErr) {
            var errorArr: Array<any> = new Array();
            resolvedPromises.map((item) => {
              if (item.success) {
                deleteImageFromGCS(item.image);
              } else {
                errorArr.push(item.error);
              }
            });
            return res.status(400).json({
              message: "Error uploading images xox",
              error: errorArr,
            });
          }
          // putting them in postgresql user table
          return createPostWithPrisma(
            body,
            res,
            resolvedPromises.map((image) => {
              return image.image;
            })
          );
        })
        .catch((error) => {
          return res.status(400).json({
            message: "Error uploading images xox",
            error: error.message,
          });
        });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
