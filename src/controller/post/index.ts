import { getPosts } from "./get-posts";
import { postFromId } from "./get-post-from-id";
import { createPost } from "./create-post";
import { postFromCate } from "./get-post-by-cat";
import { postFromUid } from "./get-post-from-uid";
import { postFromLoc } from "./get-post-by-loc";
import { updatePost } from "./update-post-info";
import { searchPosts } from "./search-post";
import { postFromIdNoAuthen } from "./get-post-from-id-noauthen";

export default {
  getPosts,
  postFromId,
  createPost,
  postFromCate,
  postFromUid,
  postFromLoc,
  updatePost,
  searchPosts,
  postFromIdNoAuthen,
};
