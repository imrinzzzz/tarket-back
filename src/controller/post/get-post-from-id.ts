import { prisma } from "../../middleware/prisma";

export const postFromId = async (req: any, res: any, _next: any) => {
  try {
    const { post, favouritePost } = prisma;
    const { currentUser } = res.locals;
    const thePost = await post.findUnique({
      where: {
        postId: parseInt(req.params.id),
      },
      include: {
        User: {
          select: {
            verificationStatus: true,
          },
        },
        Order: {
          select: {
            orderId: true,
          },
        },
      },
    });
    const favPost = await favouritePost.findMany({
      where: {
        googleId: currentUser,
      },
      select: {
        postId: true,
      },
    });
    const favPostArr = favPost.map(({ postId }) => postId);
    if (thePost) {
      let { User, Order, ...newPost } = thePost;
      return res.status(200).json({
        ...newPost,
        verificationStatus: thePost.User?.verificationStatus,
        orderId: thePost.Order.map(({ orderId }) => orderId),
        isFav: favPostArr.includes(thePost.postId),
      });
    } else {
      return res.status(404).json({ message: "No post was found" });
    }
    // res.json(allUsers);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem! ", error });
  }
};
