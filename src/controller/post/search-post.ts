import { prisma } from "./../../middleware/prisma";

export const searchPosts = async (req: any, res: any, _next: any) => {
  try {
    const { post, favouritePost } = prisma;
    const { query } = req.query;
    const { currentUser } = res.locals;

    const searchResult = await post.findMany({
      where: {
        isPaid: false,
        OR: [
          {
            postTitle: {
              contains: query,
              mode: "insensitive",
            },
          },
          {
            postDescription: {
              contains: query,
              mode: "insensitive",
            },
          },
        ],
      },
      orderBy: {
        postDatetime: "desc",
      },
      include: {
        User: {
          select: {
            verificationStatus: true,
          },
        },
        Order: {
          select: {
            orderId: true,
          },
        },
      },
    });
    const favPost = await favouritePost.findMany({
      where: {
        googleId: currentUser,
      },
      select: {
        postId: true,
      },
    });
    const favPostArr = favPost.map(({ postId }) => postId);
    if (searchResult)
      return res.status(200).json(
        searchResult.map((post) => {
          let { User, Order, ...newPost } = post;
          return {
            ...newPost,
            verificationStatus: post.User?.verificationStatus,
            orderId: post.Order.map(({ orderId }) => orderId),
            isFav: favPostArr.includes(post.postId),
          };
        })
      );
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem! ", error });
  }
};
