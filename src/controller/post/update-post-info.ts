import { prisma } from "../../middleware/prisma";
import PostValidator from "../../validations/post";

type IReq = {
  body: any;
  query: {
    postId: number;
  };
};

export const updatePost = async (req: IReq, res: any, _next: any) => {
  try {
    const { post } = prisma;
    const { body } = req;
    const { postId } = req.query;
    const validated = PostValidator.updatePost(body);
    if (typeof validated === "object")
      return res.status(422).json({
        message: "You may input wrong information",
        error: validated,
      });
    const updatedPost = await post.update({
      where: {
        postId: +postId,
      },
      data: {
        ...body,
      },
    });

    if (updatedPost) {
      return res.status(200).json({
        message: "Successfully edited the post!",
        post: updatedPost,
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
