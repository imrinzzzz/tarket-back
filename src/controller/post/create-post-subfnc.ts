import { prisma } from "../../middleware/prisma";
import PostValidator from "../../validations/post";

export const createPostWithPrisma = async (
  body: IPost,
  res: any,
  images: Array<any>
) => {
  const { post, user } = prisma;
  const validated = PostValidator.createNewPost(body);
  if (typeof validated === "object")
    return res.status(422).json({
      message: "You may input wrong information",
      error: validated,
    });

  const userExisted = user.findFirst({
    where: {
      googleId: body.googleId,
    },
  });

  if (!userExisted)
    return res.status(404).json({
      message: "User information does not exist or incorrect",
    });

  const newPost = await post.create({
    data: {
      ...body,
      postDatetime: new Date(body.postDatetime),
      postPicture: images,
      locationId: +body.locationId,
      postCategory: +body.postCategory,
      isPaid: false,
    },
  });

  if (newPost) {
    return res.status(201).json({
      message: "Created post successfully!",
      post: newPost,
    });
  } else {
    return res.status(400).json({
      message: "Error with creating post!",
    });
  }
};
