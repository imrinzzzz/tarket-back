import { prisma } from "../middleware/prisma";

export const getLoc = async (_req: any, res: any, _next: any) => {
  try {
    const { location } = prisma;
    const allLoc = await location.findMany();
    if (allLoc) {
      return res.status(200).json(allLoc);
    } else {
      return res.status(404).json({ message: "No location was found" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem! ", error });
  }
};
