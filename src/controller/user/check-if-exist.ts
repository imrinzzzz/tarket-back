import { prisma } from "../../middleware/prisma";

type IReq = {
  query: {
    type: string;
    data: string;
  };
};

export const checkIfExisted = async (req: IReq, res: any, _next: any) => {
  try {
    const { user } = prisma;
    const { data, type } = req.query;
    var existedUser;

    switch (type) {
      case "email":
        existedUser = await user.findFirst({ where: { email: data } });
        break;
      case "username":
        existedUser = await user.findFirst({ where: { username: data } });
        break;
      case "name":
        const firstName = data.split("_")[0];
        const lastName = data.split("_")[1];
        existedUser = await user.findFirst({
          where: {
            AND: [{ firstName: firstName }, { lastName: lastName }],
          },
        });
        break;
      default:
        return res.status(422).json({ message: "You input the wrong type!" });
    }

    if (existedUser)
      return res
        .status(200)
        .json({ message: "This " + type + " already exists", isExisted: true });
    else
      return res.status(200).json({
        message: "This " + type + " doesn't exist yet",
        isExisted: false,
      });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
