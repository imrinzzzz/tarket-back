import { prisma } from "../../middleware/prisma";

export const addProfilePicture = async (req: any, res: any, _next: any) => {
  try {
    const { user } = prisma;
    const { query } = req;
    const byteContent = req.file.buffer;

    if (!byteContent) {
      res.status(400).json({
        message: "no file was uploaded",
      });
    }

    const addPicture = await user.update({
      where: {
        googleId: query.uid,
      },
      data: {
        profilePicture: byteContent,
      },
    });

    if (addPicture) {
      return res.status(201).json({
        message: "uploaded profile picture successfully!",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
