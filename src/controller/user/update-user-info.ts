import { prisma } from "../../middleware/prisma";
import UserValidator from "../../validations/user";

type IReq = {
  body: IUser;
  query: {
    uid: string;
  };
};

export const updateUsers = async (req: IReq, res: any, _next: any) => {
  try {
    const { user } = prisma;
    const { body } = req;
    const { uid } = req.query;

    // validate user
    const validated = UserValidator.createNewUsers(body);
    if (typeof validated === "object")
      return res.status(422).json({
        message: "You may input wrong information",
        error: validated,
      });

    // find existing user
    var existedUser = await user.findFirst({
      where: { username: body.username },
    });
    if (existedUser)
      return res.status(409).json({ message: "Your username already exists" });

    const newUser = await user.update({
      where: {
        googleId: uid,
      },
      data: {
        ...body,
        verificationStatus: "done",
      },
    });

    if (newUser) {
      return res
        .status(201)
        .json({ message: "Create user successfully!", user: newUser });
    } else {
      return res.status(400).json({
        message: "Error creating new user xox",
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
