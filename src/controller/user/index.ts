import { updateUsers } from "./update-user-info";
import { checkIfExisted } from "./check-if-exist";
import { addProfilePicture } from "./add-pfp";
import { getUserDetail } from "./get-detail";
import { getUserAddress } from "./get-address";
import { checkIfExist as checkCreateUser } from "./check-create-user";

export default {
  updateUsers,
  checkIfExisted,
  addProfilePicture,
  getUserDetail,
  getUserAddress,
  checkCreateUser,
};
