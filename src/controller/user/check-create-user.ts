import { v4 as uuid } from "uuid";
import { prisma } from "../../middleware/prisma";
import UserValidator from "../../validations/user/";

type IReq = {
  body: {
    email: string;
  };
  header: any;
}

export const checkIfExist = async (req: IReq, res: any, _next: any) => {
  try {
    const { user } = prisma;
    const { email } = req.body;
    const { currentUser } = res.locals;

    const validated = UserValidator.checkEmail(email);
    if (typeof validated === "object")
      return res.status(422).json({
        message: "The email is not of type email",
        error: validated,
      });

    var newUser: any;

    var existedUser = await user.findFirst({ where: { email: email } });
    if (!existedUser) {
      newUser = await user.create({
        data: {
          userId: uuid(),
          email: email,
          googleId: currentUser,
          verificationStatus: "ready",
        },
      });
      if (!newUser) {
        return res.status(400).json({
          message: "Error creating new user xox",
        });
      }
    } else {
      if (existedUser.username) {
        return res.status(200).json({
          message: "User has already registered.",
          toHomepage: true,
        });
      }
      newUser = existedUser;
    }

    return res.status(200).json({
      message: "Update user's information using provided userId",
      userId: newUser.userId,
      toHomepage: false,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
