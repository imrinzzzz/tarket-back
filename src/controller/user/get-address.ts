import { prisma } from "../../middleware/prisma";

type IReq = {
  query: {
    uid: string;
  };
}

export const getUserAddress = async (req: IReq, res: any, _next: any) => {
  try {
    const { user } = prisma;
    const { uid } = req.query;

    const address = await user.findUnique({
      where: {
        googleId: uid,
      },
      select: {
        address: true,
        firstName: true,
        lastName: true,
      },
    });

    if (address) {
      return res.status(200).json(address);
    } else {
      return res.status(404).json({ message: "No user was found" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
