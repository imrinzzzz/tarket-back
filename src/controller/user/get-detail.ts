import { prisma } from "../../middleware/prisma";

type IReq = {
  query: {
    uid: string;
  };
};

export const getUserDetail = async (req: IReq, res: any, _next: any) => {
  try {
    const { user } = prisma;
    const { uid } = req.query;

    const userDetail = await user.findUnique({
      where: {
        googleId: uid,
      },
      select: {
        username: true,
        profilePicture: true,
        verificationStatus: true,
      },
    });

    if (userDetail) {
      return res.status(200).json(userDetail);
    } else {
      return res.status(404).json({ message: "No user was found" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Server has a problem!", error });
  }
};
